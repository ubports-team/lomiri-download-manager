Source: lomiri-download-manager
Section: net
Priority: optional
Build-Depends: dpkg-dev (>= 1.22.5), cmake,
               cmake-extras (>= 1.5-7~),
               debhelper-compat (= 13),
               dh-apparmor,
               doxygen,
               dbus-test-runner,
               googletest,
               graphviz,
               qtbase5-dev,
               libboost-log-dev,
               libboost-program-options-dev,
               libdbus-1-dev,
               libqt5sql5-sqlite,
               liblomiri-api-dev (>= 0.1.1),
               libgoogle-glog-dev,
               libgtest-dev (>= 1.10.0.20201025-1~),
               pkg-config,
               python3,
               qml-module-qttest,
               qtdeclarative5-dev,
               qtdeclarative5-dev-tools,
               qttools5-dev-tools,
               network-manager,
               rdfind,
               symlinks,
               systemd-dev,
               xvfb,
               xauth,
Maintainer: Debian UBports Team <team+ubports@tracker.debian.org>
Uploaders:
 Mike Gabriel <sunweaver@debian.org>,
 Marius Gripsgard <mariogrip@debian.org>,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://gitlab.com/ubports/development/core/lomiri-download-manager/
Vcs-Git: https://salsa.debian.org/ubports-team/lomiri-download-manager.git
Vcs-Browser: https://salsa.debian.org/ubports-team/lomiri-download-manager/

Package: libldm-common0t64
Provides: ${t64:Provides}
Replaces: libldm-common0
Breaks: libldm-common0 (<< ${source:Version})
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends},
Description: Lomiri Upload/Download Manager - shared library
 Lomiri Upload/Download Manager performs uploads and downloads from a
 centralized location.
 .
 This package includes the common shared library, shared between the
 client lib and the service lib.

Package: libldm-common-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libldm-common0t64 (= ${binary:Version}),
         qtbase5-dev,
         ${shlibs:Depends},
         ${misc:Depends}
Description: Lomiri Upload/Download Manager - development files
 Lomiri Uploadi/Download Manager performs uploads and downloads from a
 centralized location.
 .
 This package contains the common headers, shared between the client
 library and the daemon library.

Package: libldm-priv-common0t64
Provides: ${t64:Provides}
Replaces: libldm-priv-common0
Breaks: libldm-priv-common0 (<< ${source:Version})
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends},
         libldm-common0t64 (= ${binary:Version}),
Description: Lomiri Upload/Download Manager - shared private library
 Lomiri Upload/Download Manager performs uploads and downloads from a
 centralized location.
 .
 This package includes an auxiliary shared (but private) library.

Package: liblomiri-download-manager-common0t64
Provides: ${t64:Provides}
Replaces: liblomiri-download-manager-common0
Breaks: liblomiri-download-manager-common0 (<< ${source:Version})
Section: libs
Architecture: any
Multi-Arch: same
Depends: libldm-common0t64 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Description: QT library for Lomiri Download Manager - common shared library
 Lomiri Download Manager performs uploads and downloads from a
 centralized location.
 .
 This package includes the common shared library between the client lib
 and the service lib.

Package: liblomiri-download-manager-common-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libldm-common-dev (= ${binary:Version}),
         liblomiri-download-manager-common0t64 (= ${binary:Version}),
         qtbase5-dev,
         ${shlibs:Depends},
         ${misc:Depends}
Description: QT library for Lomiri Download Manager - common development files
 Lomiri Upload/Download Manager performs uploads and downloads from a
 centralized location.
 .
 This package contains the common headers shared between the client
 library and the daemon library.

Package: liblomiri-download-manager-client0t64
Provides: ${t64:Provides}
Replaces: liblomiri-download-manager-client0
Breaks: liblomiri-download-manager-client0 (<< ${source:Version})
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends},
         libldm-common0t64 (= ${binary:Version}),
         liblomiri-download-manager-common0t64 (= ${binary:Version}),
Description: QT library for Lomiri Download Manager - shared client library
 Lomiri Download Manager performs downloads from a centralized location.
 .
 This package includes the public shared library providing the API
 for implementing a service client.

Package: liblomiri-download-manager-client-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libldm-common-dev (= ${binary:Version}),
         liblomiri-download-manager-common-dev (= ${binary:Version}),
         liblomiri-download-manager-client0t64 (= ${binary:Version}),
         qtbase5-dev,
         ${shlibs:Depends},
         ${misc:Depends}
Breaks: lomiri-download-manager-client-dev
Replaces: lomiri-download-manager-client-dev
Description: QT library for Lomiri Download Manager - client development files
 Lomiri Download Manager performs downloads from a centralized location.
 .
 This package contains the header that can be used to allow an
 application use the Lomiri Download Manager client library.

Package: liblomiri-download-manager-client-doc
Section: doc
Architecture: all
Depends: liblomiri-download-manager-client-dev (>= ${source:Version}),
          ${misc:Depends},
Description: QT library for Lomiri Download Manager - client documentation files
 Lomiri Upload/Download Manager performs uploads and downloads from a
 centralized location.
 .
 Documentation files for the LDM client development.

Package: liblomiri-upload-manager-common0t64
Provides: ${t64:Provides}
Replaces: liblomiri-upload-manager-common0
Breaks: liblomiri-upload-manager-common0 (<< ${source:Version})
Section: libs
Architecture: any
Multi-Arch: same
Depends: libldm-common0t64 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Description: QT library for Lomiri Upload Manager - shared common library
 Lomiri Upload Manager performs uploads from a centralized location.
 .
 This package includes the common shared library between the client lib
 and the service lib.

Package: liblomiri-upload-manager-common-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libldm-common-dev (= ${binary:Version}),
         liblomiri-upload-manager-common0t64 (= ${binary:Version}),
         qtbase5-dev,
         ${shlibs:Depends},
         ${misc:Depends}
Description: QT library for Lomiri Upload Manager - common development files
 Lomiri Upload Manager performs uploads from a centralized location.
 .
 This package contains the common headers shared between the client
 library and the daemon library.

Package: lomiri-download-manager
Architecture: any
Multi-Arch: foreign
Depends: ${misc:Depends},
         ${shlibs:Depends},
         libldm-common0t64 (= ${binary:Version}),
         libldm-priv-common0t64 (= ${binary:Version}),
         liblomiri-download-manager-common0t64 (= ${binary:Version}),
         unzip
Suggests: apparmor
Description: Lomiri Download Manager - daemon
 Lomiri Download Manager performs downloads from a centralized
 location.
 .
 This package includes the Lomiri Download Manager user space daemon.

Package: lomiri-upload-manager
Architecture: any
Multi-Arch: foreign
Depends: ${misc:Depends},
         ${shlibs:Depends},
         libldm-common0t64 (= ${binary:Version}),
         libldm-priv-common0t64 (= ${binary:Version}),
         liblomiri-upload-manager-common0t64 (= ${binary:Version}),
Description: Lomiri Upload Manager - daemon
 Lomiri Upload Manager performs uploads from a centralized
 location.
 .
 This package includes the Lomiri Upload Manager user space daemon.

Package: qml-module-lomiri-downloadmanager
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends},
         libldm-common0t64 (= ${binary:Version}),
         liblomiri-download-manager-common0t64 (= ${binary:Version}),
         liblomiri-download-manager-client0t64 (= ${binary:Version}),
         lomiri-download-manager (= ${binary:Version})
Description: Lomiri Download Manager QML Plugin
 Lomiri Upload/Download Manager performs uploads and downloads from a
 centralized location.
 .
 This package contains a QML Plugin to handle downloads from a pure QML
 application, without the need to write any C++ code.

Package: qml-module-lomiri-downloadmanager-doc
Section: doc
Architecture: all
Depends: ${misc:Depends},
Description: Lomiri Download Manager QML Plugin - documentation files
 Lomiri Upload/Download Manager performs uploads and downloads from a
 centralized location.
 .
 This package contains the documentation of the Lomiri DownloadManager
 QML Plugin.
